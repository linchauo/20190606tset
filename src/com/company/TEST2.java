package com.company;

import java.util.Scanner;

public class TEST2 {
    public static void main(String[] args) {
        int n[] = new int[100], k = 0;
        int num = 0;
        float avg = 0;
        System.out.println("請輸入多個數字");
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            n[k] = scanner.nextInt();
            if (n[k] == 0) {
                break;
            }
            k++;
        }
        for (int i = 0; i < k; i++) {
            System.out.print(n[i]+" ");
            num += n[i];
        }
        avg+=(float)num/k;
        System.out.println("\n總和:"+num);
        System.out.println("平均:"+avg);
    }
}
