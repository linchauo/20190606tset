package com.company;

import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class test4 {
    public static void main(String[] args) {
        int k = 0;
        int n = ThreadLocalRandom.current().nextInt(10, 21);
        Scanner scanner = new Scanner(System.in);
        char U_D = scanner.next().charAt(0);
        System.out.println("請輸入" + n + "個數字");
        int num[] = new int[n];
        while (scanner.hasNext()) {
            num[k++] = scanner.nextInt();
            if (k == n) {
                break;
            }
        }
        Arrays.sort(num);
        if (U_D == 'D')
            for (int i = k-1; i >= 0; i--)
                System.out.print(num[i] + " ");
        if (U_D == 'U')
            for (int i = 0; i < k; i++)
                System.out.print(num[i] + " ");
    }
}